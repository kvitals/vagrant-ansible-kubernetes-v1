FROM node:14-alpine AS build
ARG commit
ENV REACT_APP_CI_COMMIT_SHA=$commit
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --no-optional --no-progress
COPY . .
RUN npm test -- --watchAll=false \
 && NODE_ENV=production npm run build


FROM nginx:alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html

