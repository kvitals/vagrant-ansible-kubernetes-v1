After you clone the project,<br>
In the project directory, you can run the command:

### `vagrant up`

When machine will be up & running, check the app in browser using VM's IP:<br>

`vagrant ssh -c "hostname -I | cut -d' ' -f2"`
