import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const commitid = process.env.REACT_APP_CI_COMMIT_SHA ?? 'latest'

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href={`https://gitlab.com/kvitals/demo-app/-/commit/${commitid}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {`Latest Demo App commit ID: ${commitid}`}
        </a>
      </header>
    </div>
  );
}

export default App;
