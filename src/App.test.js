import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders commit link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/commit/i);
  expect(linkElement).toBeInTheDocument();
});
